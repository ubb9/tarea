#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#define MAX_IP_LEN 40
#define MAX_COMMAND_LEN 100
#define MAX_LINE_LEN 255
using namespace std;

    void leerArchivo(const char *archivo);
    void recorrerPing(const char *ip,int c1);

    int main(void)
    {
        char opcion;
        char archivo[MAX_LINE_LEN];

                printf("\n Introducir nombre archivo txt: ");
                scanf("%s", archivo);
                leerArchivo(archivo);
        return 0;
    }

    void leerArchivo(const char *archivo)
    {
        FILE * f;
        char ip[MAX_IP_LEN] = { 0 };

        f = fopen(archivo, "rt");
        if (f == NULL)
        {
            printf("Problemas de apertura del archivo\n");
        }
        else
        {
           int c1;
           printf("Ingrese cantidad de paquetes a trasnmitir: ");
           scanf("%d",&c1);
            while (fgets(ip, MAX_IP_LEN, f))
            {
                recorrerPing(ip,c1);
            }

            fclose(f);
        }
    }
void recorrerPing(const char *ip,int c1)
    {
        char line[MAX_LINE_LEN] = { 0 };
        char command[MAX_COMMAND_LEN] = { 0 };
        FILE * fp;

        snprintf(command, MAX_COMMAND_LEN, "ping -c%d %s ",c1, ip);
        fp = popen(command, "r");
        if (fp == NULL)
        {
            return;
        }
        while (fgets(line, MAX_LINE_LEN, fp) != NULL)
        {
            printf("%s", line);
        }

        pclose(fp);
    }

